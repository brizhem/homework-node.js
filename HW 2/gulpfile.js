const gulp = require('gulp');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const minifyjs = require('gulp-js-minify');

gulp.task('clean', () => {
    return gulp.src('dist', { read: false, allowEmpty: true })
        .pipe(clean());
});

gulp.task('buildCss', () => {
    return gulp
        .src('src/styles/**.*scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(autoprefixer({ cascade: false }))
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('buildJs', () => {
    return gulp
        .src('src/js/**.*js')
        .pipe(concat('scripts.min.js'))
        .pipe(minifyjs())
        .pipe(gulp.dest('dist'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('imgOptimization', () => {
    return gulp.src('src/img/*')
        .pipe(imagemin([imagemin.mozjpeg({ quality: 75, progressive: true })]))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('dev', function() {
	browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    gulp.watch('src/**/*.*',
            gulp.series('clean',
            gulp.parallel('buildCss', 'imgOptimization', 'buildJs'))).on('change', browserSync.reload);
});


gulp.task('build', (done) => {
    gulp.series('clean',
        gulp.parallel('buildCss', 'imgOptimization', 'buildJs'));
    
    done();
});