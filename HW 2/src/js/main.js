const elMenuBtn = document.querySelector('.menu-btn');
const elNavList = document.querySelector('.nav-list');

elMenuBtn.addEventListener('click', (e) => {
    e.target.classList.toggle('open');
    elNavList.classList.toggle('open');
});